//
//  ProfileViewController.swift
//  tercer-obligatorio-ios
//
//  Created by SP19 on 9/6/16.
//  Copyright © 2016 UCU. All rights reserved.
//

import UIKit
import ObjectMapper
import Kingfisher
import TSMessages

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var birthDayLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mobilePhoneTextField: UITextField!

    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2
        profileImageView.layer.borderWidth = 2
        profileImageView.layer.borderColor = UIColor.whiteColor().CGColor
        profileImageView.clipsToBounds = true
        loadProfile()
    }

    @IBAction func saveProfile(sender: AnyObject) {
        if let usrId = StorageUtils.sharedInstance.getClassmate() {
            FirebaseDataService.sharedInstance.CLASSMATE_REF.queryOrderedByChild("id").queryEqualToValue(usrId).observeSingleEventOfType(.Value, withBlock: { snapshot in
                
                if !(snapshot.value is NSNull) {
                    let dict = snapshot.value as! [String: AnyObject]
                    let array = Array(dict.values)
                    var usr = Mapper<ClassMate>().map(array.first)!
                    
                    usr.cellPhone = self.mobilePhoneTextField.text
                    usr.email = self.emailTextField.text
                    FirebaseDataService.sharedInstance.CLASSMATE_REF.child(usrId).setValue(Mapper<ClassMate>().toJSONDictionary(["classmate" : usr])["classmate"])
                    
                    TSMessage.showNotificationInViewController(self, title: "Success", subtitle: "Profile saved!", type: TSMessageNotificationType.Success)

                }
            })
        }
    }
    
    @IBAction func logOut(sender: AnyObject) {
        StorageUtils.sharedInstance.logout()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func loadProfile() {
        if let usrId = StorageUtils.sharedInstance.getClassmate() {
            self.loadingActivityIndicator.startAnimating()
            FirebaseDataService.sharedInstance.CLASSMATE_REF.queryOrderedByChild("id").queryEqualToValue(usrId).observeEventType(.Value, withBlock: { snapshot in
                    
                if !(snapshot.value is NSNull) {
                    let dict = snapshot.value as! [String: AnyObject]
                    let array = Array(dict.values)
                    let usr = Mapper<ClassMate>().map(array.first)!
                    self.mobilePhoneTextField.text = usr.cellPhone
                    self.emailTextField.text = usr.email
                    self.birthDayLabel.text = usr.birthday
                    self.infoLabel.text = "\(usr.name!) \(usr.lastName!)"

                    var imgEncode: String? = usr.imageEncoded
                    let decodedData = NSData(base64EncodedString: imgEncode! as String, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)
                    var decodedimage = UIImage(data: decodedData!)
                    
                    
                    if usr.imageUrl != "" {
                        self.profileImageView.kf_setImageWithURL(NSURL(string: usr.imageUrl!)!, placeholderImage: UIImage(named: "profle-512"))
                    } else {
                        if(decodedimage != nil){
                            self.profileImageView.image = decodedimage! as UIImage
                        }
                        else{
                            self.profileImageView.image = UIImage(named: "profle-512")
                        }
                    }
                    self.loadingActivityIndicator.stopAnimating()
                  
                }
            })
            
        }
    }
}