//
//  LoginViewController.swift
//  tercer-obligatorio-ios
//
//  Created by SP19 on 9/6/16.
//  Copyright © 2016 UCU. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViewController: UIViewController,  FBSDKLoginButtonDelegate {

    @IBOutlet weak var loginFacebookButton: FBSDKLoginButton!

    var firstName: String?
    var lastName: String?
    var strPictureURL: String?
    var email: String?
    var birthday: String?
    var phone: String?
    var imgeEncoded:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureFacebook()
    }
    
    @IBAction func signUp(sender: AnyObject) {
        performSegueWithIdentifier("SignUp", sender: self)
    }
    
    func configureFacebook() {
        self.loginFacebookButton.readPermissions = ["public_profile", "email", "user_friends", "user_birthday",];
        self.loginFacebookButton.delegate = self
        
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields":"first_name, last_name, email, birthday, picture.type(large)"]).startWithCompletionHandler { (connection, result, error) -> Void in
           print(result)
            self.firstName = result.objectForKey("first_name") as? String    
            self.lastName = result.objectForKey("last_name") as? String
            self.email = result.objectForKey("email") as? String
            self.birthday = result.objectForKey("birthday") as? String
            self.strPictureURL = (result.objectForKey("picture")?.objectForKey("data")?.objectForKey("url") as? String)!

            dispatch_after(dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(1.0 * Double(NSEC_PER_SEC))
                ), dispatch_get_main_queue(), { () -> Void in
                    self.performSegueWithIdentifier("SignUpFB", sender: self)                    
            })
        }
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "SignUpFB"){
            let signUpViewController = segue.destinationViewController as! SignUpViewController

            let newUser = ClassMate(id: "1", name: self.firstName!, lastName: self.lastName!, email: self.email!, cellPhone: "", img: self.strPictureURL!, birth: self.birthday!, imageEncoded: "")
            
            signUpViewController.user = newUser
                        
        }
    }

}
