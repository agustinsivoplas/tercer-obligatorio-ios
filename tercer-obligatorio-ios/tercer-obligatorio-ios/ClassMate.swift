//
//  ClassMate.swift
//  tercer-obligatorio-ios
//
//  Created by SP19 on 15/6/16.
//  Copyright © 2016 UCU. All rights reserved.
//

import Foundation
import ObjectMapper

class ClassMate: Mappable {
    
    var id: String?
    var name: String?
    var email: String?
    var cellPhone: String?
    var imageUrl: String?
    var lastName:String?
    var birthday: String?
    var imageEncoded: String?
    var subjetsEnrolled: [Subjet] = []
    
    init(id: String, name n:String, lastName ln:String, email e:String, cellPhone cell:String, img imageUrl: String, birth birthday: String, imageEncoded:String) {
        self.id = id
        self.name = n
        self.lastName = ln
        self.email = e
        self.cellPhone = cell
        self.imageUrl = imageUrl
        self.birthday = birthday
        self.imageEncoded = imageEncoded
    }
    
    
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.name <- map["name"]
        self.email <- map["email"]
        self.cellPhone <- map["cellPhone"]
        self.lastName <- map["lastName"]
        self.imageUrl <- map["imageUrl"]
        self.birthday <- map["birthday"]
        self.subjetsEnrolled <- map["subjectsEnrolled"]
        self.imageEncoded <- map["imageEncoded"]
    }
}