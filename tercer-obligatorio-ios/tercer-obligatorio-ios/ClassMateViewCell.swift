//
//  ClassMateViewCell.swift
//  tercer-obligatorio-ios
//
//  Created by SP19 on 10/6/16.
//  Copyright © 2016 UCU. All rights reserved.
//

import UIKit

class ClassMateViewCell: UITableViewCell {

    @IBOutlet weak var classmateImageView: UIImageView!
    @IBOutlet weak var cellPhoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
}
