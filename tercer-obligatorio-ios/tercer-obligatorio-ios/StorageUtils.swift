//
//  File.swift
//  tercer-obligatorio-ios
//
//  Created by SP20 on 17/6/16.
//  Copyright © 2016 UCU. All rights reserved.
//

import Foundation

class StorageUtils {
    
    static let sharedInstance = StorageUtils()
    let defaults = NSUserDefaults.standardUserDefaults()
    
    private init() {
        
    }
    
    func setClassmate(userId: String) {
        defaults.setValue(userId, forKey: "logger")
    }
    
    func getClassmate() -> String? {
        if let value = defaults.valueForKey("logger") {
            return value as! String
        }
        return nil
    }
    
    func logout() {
        defaults.removeObjectForKey("logger")
    }
}