//
//  SubjetCourseCollectionViewCell.swift
//  tercer-obligatorio-ios
//
//  Created by SP19 on 24/6/16.
//  Copyright © 2016 UCU. All rights reserved.
//

import UIKit

class SubjetCourseCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var subjetImageView: UIImageView!
    @IBOutlet weak var subjetTitle: UILabel!
    
    override var selected: Bool {
        didSet {
            if selected {
                subjetImageView.layer.borderWidth = 2.0
                subjetImageView.layer.borderColor = UIColor.redColor().CGColor
            } else {
                subjetImageView.layer.borderWidth = 0.0
            }
        }
    }
}
