//
//  EnrollSubjetViewController.wift
//  tercer-obligatorio-ios
//
//  Created by SP19 on 9/6/16.
//  Copyright © 2016 UCU. All rights reserved.
//

import UIKit
import ObjectMapper
import TSMessages

class EnrollSubjetViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var collectionViewDataSource:[Subjet] = []
    
    var lastRowCollectionShow:Int = -1
    var delay = 0.5
    
    @IBOutlet weak var pwdTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.allowsMultipleSelection = false
        loadSubjects()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func enrollMeClick(sender: AnyObject) {
        if let selectedSubjetIndex = self.collectionView.indexPathsForSelectedItems()?.first {
            let passwd = self.pwdTextField.text
            if passwd == "agendaT" {
                
                if let usrId = StorageUtils.sharedInstance.getClassmate() {
                    FirebaseDataService.sharedInstance.CLASSMATE_REF.queryOrderedByChild("id").queryEqualToValue(usrId).observeSingleEventOfType(.Value, withBlock: { snapshot in
                        
                        if !(snapshot.value is NSNull) {
                            let dict = snapshot.value as! [String: AnyObject]
                            let array = Array(dict.values)
                            var usr = Mapper<ClassMate>().map(array.first)!
                            var alreadyEnrolled = false
                            
                            let selectedSubjet = self.collectionViewDataSource[selectedSubjetIndex.row]
                            
                            for subj in usr.subjetsEnrolled {
                                if subj.id == selectedSubjet.id {
                                    alreadyEnrolled = true
                                }
                            }
                            
                            if(alreadyEnrolled) {
                                TSMessage.showNotificationInViewController(self, title: "Warning", subtitle: "User already enrolled in \(selectedSubjet.description!)", type: TSMessageNotificationType.Warning)
                            } else {
                                usr.subjetsEnrolled.append(selectedSubjet)
                                FirebaseDataService.sharedInstance.CLASSMATE_REF.child(usrId).setValue(Mapper<ClassMate>().toJSONDictionary(["classmate" : usr])["classmate"])
                                TSMessage.showNotificationInViewController(self, title: "Success", subtitle: "User enrolled succesfully", type: TSMessageNotificationType.Success)

                                
                            }
                        }
                    })
                }
                
            } else{
                TSMessage.showNotificationInViewController(self, title: "Error", subtitle: "Invalid password", type: TSMessageNotificationType.Error)
            }
        } else {
            TSMessage.showNotificationInViewController(self, title: "Error", subtitle: "You must select a subjet.", type: TSMessageNotificationType.Error)
        }
    }
        
    func loadSubjects() {
        FirebaseDataService.sharedInstance.SUBJET_REF.queryOrderedByChild("description").observeEventType(.Value, withBlock: { snapshot in
            
            let dict = snapshot.value as! [String: AnyObject]
            
            let array = Array(dict.values)
            
            self.collectionViewDataSource = Mapper<Subjet>().mapArray(array)!
            
            self.collectionView.reloadData()
            }, withCancelBlock: { error in
                print(error.description)
        })
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionViewDataSource.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cellSubjet", forIndexPath: indexPath) as! SubjetCollectionViewCell
        
        let subjet = self.collectionViewDataSource[indexPath.row]
        
        cell.subjetLabel.text = subjet.description
        cell.subjetImageView.kf_setImageWithURL(NSURL(string: subjet.imageUrl!)!)
        
        cell.subjetImageView.layer.cornerRadius = cell.subjetImageView.frame.size.width / 2.0
        cell.subjetImageView.layer.borderWidth = 1
        cell.subjetImageView.layer.borderColor = UIColor.grayColor().CGColor
        cell.subjetImageView.clipsToBounds = true
        cell.subjetImageView.contentMode = UIViewContentMode.ScaleAspectFit
        
        if(indexPath.row > self.lastRowCollectionShow){
            self.lastRowCollectionShow = indexPath.row
            cell.transform = CGAffineTransformMakeScale(0, 0)
            if(indexPath.row < 3){
                delay = Double(indexPath.row)/6
            }
            
            UIView.animateWithDuration(1, delay: delay, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.6, options: .CurveEaseInOut, animations: {() -> Void in
                cell.transform = CGAffineTransformIdentity}, completion: nil)
        }

        return cell
    }
    
}

