//
//  Subjet.swift
//  tercer-obligatorio-ios
//
//  Created by SP19 on 15/6/16.
//  Copyright © 2016 UCU. All rights reserved.
//

import Foundation
import ObjectMapper

class Subjet: Mappable {
    
    var id: String?
    var description: String?
    var imageUrl: String?
    
    required init?(_ map: Map) {
    }
    
    init(id:String, desc description:String, img imageUrl:String) {
        self.id = id
        self.description = description
        self.imageUrl = imageUrl
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.description <- map["description"]
        self.imageUrl <- map["imageUrl"]
    }

}
