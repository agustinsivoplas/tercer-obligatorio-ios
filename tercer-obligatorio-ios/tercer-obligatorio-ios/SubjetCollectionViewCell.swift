//
//  SubjetCollectionViewCell.swift
//  tercer-obligatorio-ios
//
//  Created by SP19 on 17/6/16.
//  Copyright © 2016 UCU. All rights reserved.
//

import UIKit

class SubjetCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var subjetImageView: UIImageView!
    @IBOutlet weak var subjetLabel: UILabel!
    
    override var selected: Bool {
        didSet {
            if selected {
                subjetImageView.layer.borderWidth = 2.0
                subjetImageView.layer.borderColor = UIColor.redColor().CGColor
            } else {
                subjetImageView.layer.borderWidth = 0.0
            }
        }
    }}
