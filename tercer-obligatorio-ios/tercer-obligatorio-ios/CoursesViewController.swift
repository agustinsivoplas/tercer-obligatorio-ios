//
//  CoursesViewController.swift
//  tercer-obligatorio-ios
//
//  Created by SP19 on 9/6/16.
//  Copyright © 2016 UCU. All rights reserved.
//

import UIKit
import ObjectMapper
import Foundation
import Kingfisher

class CoursesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var subjectsCollectionView: UICollectionView!
    @IBOutlet weak var classMatesTableView: UITableView!
    
    var subjetDataSource:[Subjet] = []
    var classMatesDataSource:[ClassMate] = []
    
    var lastRowCollectionShow:Int = -1
    var delay = 0.5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.subjectsCollectionView.delegate = self
        self.subjectsCollectionView.dataSource = self
        
        self.classMatesTableView.delegate = self
        self.classMatesTableView.dataSource = self
    }
    
    @IBAction func enrollButton(sender: AnyObject) {
        performSegueWithIdentifier("showEnrollSegue", sender: nil)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        loadSubjects()
    }
    
    func loadSubjects() {
        if let usrId = StorageUtils.sharedInstance.getClassmate() {
            FirebaseDataService.sharedInstance.CLASSMATE_REF.queryOrderedByChild("id").queryEqualToValue(usrId).observeEventType(.Value, withBlock: { snapshot in

                if !(snapshot.value is NSNull) {
                    let dict = snapshot.value as! [String: AnyObject]
                    let array = Array(dict.values)
                    let usr = Mapper<ClassMate>().map(array.first)!
                    self.subjetDataSource = usr.subjetsEnrolled
                    
                    self.subjectsCollectionView.reloadData()
                    if !self.subjetDataSource.isEmpty {
                        self.loadClassmates(self.subjetDataSource.first!)
                    }
                }
            })
        }
    }
    
    func loadClassmates(subject: Subjet) {
       FirebaseDataService.sharedInstance.CLASSMATE_REF.queryOrderedByChild("lastName").observeEventType(.Value, withBlock: { snapshot in
            
            let dict = snapshot.value as! [String: AnyObject]
            
            let array = Array(dict.values)

            self.classMatesDataSource.removeAll()

            let classMateArray = Mapper<ClassMate>().mapArray(array)!
            for cm in classMateArray {
                for sub in cm.subjetsEnrolled {
                    if sub.id == subject.id {
                        self.classMatesDataSource.append(cm)
                    }
                }
            
            }
            
            self.classMatesTableView.reloadData()

            }, withCancelBlock: { error in
                print(error.description)
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return classMatesDataSource.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("classMateCellId", forIndexPath: indexPath) as! ClassMateViewCell
        
        let classmate = self.classMatesDataSource[indexPath.row]
        
        cell.nameLabel.text = "\(classmate.name!) \(classmate.lastName!)"
        cell.emailLabel.text = classmate.email
        cell.cellPhoneLabel.text = classmate.cellPhone
        
        if(classmate.imageUrl != ""){
            cell.classmateImageView.kf_setImageWithURL(NSURL(string: classmate.imageUrl!)!)
        }
        else{
            var imgEncode: String? = classmate.imageEncoded
            let decodedData = NSData(base64EncodedString: imgEncode! as String, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)
            var decodedimage = UIImage(data: decodedData!)
            cell.classmateImageView.image = decodedimage
        
        }
        
        cell.classmateImageView.layer.cornerRadius = cell.classmateImageView.frame.size.width / 2.0
        cell.classmateImageView.layer.borderWidth = 1
        cell.classmateImageView.layer.borderColor = UIColor.grayColor().CGColor
        cell.classmateImageView.clipsToBounds = true
        
        cell.transform = CGAffineTransformMakeScale(0, 0)
            
        UIView.animateWithDuration(1, delay: delay, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.6, options: .CurveEaseInOut, animations: {() -> Void in
                cell.transform = CGAffineTransformIdentity}, completion: nil)
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.subjetDataSource.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        var numOfSection: NSInteger = 0
        
        if subjetDataSource.count > 0 {
            self.subjectsCollectionView.backgroundView = nil
            numOfSection = 1
        } else {
            
            var noDataLabel: UILabel = UILabel(frame: CGRectMake(0, 0, self.subjectsCollectionView.bounds.size.width, self.subjectsCollectionView.bounds.size.height))
            noDataLabel.text = "Not enrolled in any subjet"
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.textAlignment = NSTextAlignment.Center
            self.subjectsCollectionView.backgroundView = noDataLabel
            
        }
        return numOfSection
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cellSubjet", forIndexPath: indexPath) as! SubjetCourseCollectionViewCell
        
        let subjet = self.subjetDataSource[indexPath.row]
        
        cell.subjetTitle.text = subjet.description
        cell.subjetImageView.kf_setImageWithURL(NSURL(string: subjet.imageUrl!)!)
        
        cell.subjetImageView.layer.cornerRadius = cell.subjetImageView.frame.size.width / 2.0
        cell.subjetImageView.layer.borderWidth = 1
        cell.subjetImageView.layer.borderColor = UIColor.grayColor().CGColor
        cell.subjetImageView.clipsToBounds = true
        cell.subjetImageView.contentMode = UIViewContentMode.ScaleAspectFit
        
        if(indexPath.row > self.lastRowCollectionShow){
            self.lastRowCollectionShow = indexPath.row
            cell.transform = CGAffineTransformMakeScale(0, 0)
            if(indexPath.row < 3){
                delay = Double(indexPath.row)/6
            }
            
            UIView.animateWithDuration(1, delay: delay, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.6, options: .CurveEaseInOut, animations: {() -> Void in
                cell.transform = CGAffineTransformIdentity}, completion: nil)
        }
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        var numOfSection: NSInteger = 0
        if classMatesDataSource.count > 0 {
            
            self.classMatesTableView.backgroundView = nil
            numOfSection = 1
            
        } else {
            
            var noDataLabel: UILabel = UILabel(frame: CGRectMake(0, 0, self.classMatesTableView.bounds.size.width, self.classMatesTableView.bounds.size.height))
            noDataLabel.text = "No Data Available :("
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.textAlignment = NSTextAlignment.Center
            self.classMatesTableView.backgroundView = noDataLabel
            
        }
        return numOfSection
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.loadClassmates(self.subjetDataSource[indexPath.row])
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
}
