//
//  SplashViewController.swift
//  tercer-obligatorio-ios
//
//  Created by SP19 on 9/6/16.
//  Copyright © 2016 UCU. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    var userId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        userId = StorageUtils.sharedInstance.getClassmate()
        var logged = false
        
        if(self.userId != nil){
            logged = true
        }

        if logged {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (Int64)(1 * NSEC_PER_SEC)), dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("showCoursesSegue", sender: nil)
            }
            
        } else {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (Int64)(1 * NSEC_PER_SEC)), dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("showLoginSegue", sender: nil)
            }
            
        }

    }
}