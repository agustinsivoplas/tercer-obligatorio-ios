//
//  SignUpViewController.wift
//  tercer-obligatorio-ios
//
//  Created by SP19 on 9/6/16.
//  Copyright © 2016 UCU. All rights reserved.
//

import UIKit
import TSMessages
import Kingfisher

class SignUpViewController: UIViewController, UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate {
    
    
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var birthday: UITextField!
    
    var picker:UIImagePickerController?=UIImagePickerController()
    var user: ClassMate?
    var imgEncoded:String?
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    ////// Open Gallery //////
    @IBAction func openGallery(sender: AnyObject) {
        self.openGallary()
    }
    
    func openGallary() {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        presentViewController(picker!, animated: true, completion: nil)
    }
    
    ////// Take a Photo //////
    @IBAction func takePhoto(sender: AnyObject) {
        self.openCamera()
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerControllerSourceType.Camera
            picker!.cameraCaptureMode = .Photo
            presentViewController(picker!, animated: true, completion: nil)
        } else{
 
            TSMessage.showNotificationInViewController(self, title: "Warning", subtitle: "This device has not Camera", type: TSMessageNotificationType.Warning)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imageProfile.layer.borderWidth = 1
        self.imageProfile.layer.masksToBounds = false
        self.imageProfile.layer.borderColor = UIColor.whiteColor().CGColor
        self.imageProfile.layer.cornerRadius = self.imageProfile.frame.height/2
        self.imageProfile.clipsToBounds = true
        
        self.firstName.text = user?.name
        self.lastName.text = user?.lastName
        self.phoneNumber.text = user?.cellPhone
        self.email.text = user?.email
        self.birthday.text = user?.birthday
        if(user?.imageUrl != nil){
            self.imageProfile.kf_setImageWithURL(NSURL(string: (user?.imageUrl)!)!)
        }
        else{
            self.imageProfile.image = UIImage(named:"profle-512")!
        }

        picker?.delegate=self
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        self.imageProfile.layer.borderWidth = 1
        self.imageProfile.layer.masksToBounds = false
        self.imageProfile.layer.borderColor = UIColor.whiteColor().CGColor
        self.imageProfile.layer.cornerRadius = self.imageProfile.frame.height/2
        self.imageProfile.clipsToBounds = true
        
        imageProfile.image = self.resizeImage(chosenImage, targetSize: CGSizeMake(200.0, 200.0))
        //let imageData:NSData = UIImageJPEGRepresentation(chosenImage, 0.2)!
        let imageData:NSData = UIImagePNGRepresentation(self.resizeImage(chosenImage, targetSize: CGSizeMake(200.0, 200.0)))!
        let strBase64:String = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        self.imgEncoded = strBase64

        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func SignUp(sender: AnyObject) {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        let isValidEmail = emailPredicate.evaluateWithObject(self.email.text)
        if (!isValidEmail){
            TSMessage.showNotificationInViewController(self, title: "Error", subtitle: "Enter a valid email", type: TSMessageNotificationType.Error)
        } else {
            self.activityIndicator.startAnimating()
            
            var imageProfileUrl = ""
            var imageProfileGallery = ""

            if (self.user == nil){
                if(self.imgEncoded !=  nil){
                    imageProfileGallery = self.imgEncoded!
                }
                else{
                    let imageData:NSData = UIImagePNGRepresentation(self.resizeImage(UIImage(named: "profle-512")!, targetSize: CGSizeMake(200.0, 200.0)))!
                    let strBase64:String = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
                    self.imgEncoded = strBase64
                    
                    imageProfileGallery = strBase64
                }
            }
            else{
                imageProfileUrl = (self.user?.imageUrl)!
            }
 
            var classMate = ClassMate(id: NSUUID().UUIDString, name: self.firstName.text!, lastName: self.lastName.text!, email: self.email.text!, cellPhone: self.phoneNumber.text!, img: imageProfileUrl, birth: self.birthday.text!, imageEncoded: imageProfileGallery)
        
            FirebaseDataService.sharedInstance.createClassmate(classMate)
            TSMessage.showNotificationInViewController(self, title: "Success", subtitle: "", type: TSMessageNotificationType.Success)
            
            StorageUtils.sharedInstance.setClassmate(classMate.id!)
            
            dispatch_after(dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(2.0 * Double(NSEC_PER_SEC))
                ), dispatch_get_main_queue(), { () -> Void in
                    self.performSegueWithIdentifier("EnrollSignature", sender: self)
            })
        }
    }
}

