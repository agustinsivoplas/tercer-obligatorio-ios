//
//  FirebaseDataService.swift
//  tercer-obligatorio-ios
//
//  Created by SP19 on 15/6/16.
//  Copyright © 2016 UCU. All rights reserved.
//

import Foundation
import Firebase
import ObjectMapper

class FirebaseDataService {
    
    static let sharedInstance = FirebaseDataService()
    
    let ref = FIRDatabase.database().reference()
    let SUBJET_CHILD = "subjets"
    let CLASSMATES_CHILD = "classmates"
    let SUBJET_ENROLL_CHILD = "subject_enroll"
    
    private init() {
        
    }
    
    var SUBJET_REF: FIRDatabaseReference {
        return ref.child(SUBJET_CHILD)
    }
    
    var CLASSMATE_REF: FIRDatabaseReference {
        return ref.child(CLASSMATES_CHILD)
    }
    
    func createSubjet(subjet:Subjet) {
        
        ref.child(SUBJET_CHILD).child(subjet.id!).setValue(Mapper<Subjet>().toJSONDictionary(["subject" : subjet])["subject"])
    }
    
    func createClassmate(classmate:ClassMate) {
        ref.child(CLASSMATES_CHILD).child(classmate.id!).setValue(Mapper<ClassMate>().toJSONDictionary(["classmate" : classmate])["classmate"])
    }
    
    func enrollToSubjet(subject:Subjet) {
        if let usrId = StorageUtils.sharedInstance.getClassmate() {
            CLASSMATE_REF.queryOrderedByChild("id").queryEqualToValue(usrId).observeSingleEventOfType(.Value, withBlock: { snapshot in

                if !(snapshot.value is NSNull) {
                    let dict = snapshot.value as! [String: AnyObject]
                    let array = Array(dict.values)
                    var usr = Mapper<ClassMate>().map(array.first)!
                    usr.subjetsEnrolled.append(subject)
                    self.CLASSMATE_REF.child(usrId).setValue(Mapper<ClassMate>().toJSONDictionary(["classmate" : usr])["classmate"])
                }
            })
        }
    }
    
    func listClassMates(subjet: Subjet) -> Array<ClassMate> {
        return []
    }
    
}
